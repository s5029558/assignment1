
#Assignment 1 - Software Technologies - 7810ICT

#Walter Meza Almeida s5029558
#Mauricio Gonzalez Betancur s5030962

import sys
from Functions import *

fname = input("Enter dictionary name followed by .txt: ")  # the user inputs the dictionary name. Errors can occur
while not (isFile(fname)):  # if the file doesn't exist, print a message
    if (fname == "-1"): # this name is not common and it is not followed by .txt
        print("  Thank you, have a nice day")
        sys.exit(0)
    else:
        print("  Wrong file name, please try again or enter -1 to exit")#
    fname = input("Enter dictionary name followed by .txt: ")  # the user can input the dictionary name again in case or errors.

file = open(fname)  # open the dictionary file
lines = file.readlines()  # read the file lines

inputs = enterwords(lines)
start = inputs[0]
target = inputs[1]
words = inputs[2]

while not (isValid(words, [start, target])):  # Check whether the start word and the target word exist
    if (start == "-1" or target == "-1"): # this name is not common and it is not followed by .txt
        print("  Thank you, have a nice day")
        sys.exit(0)
    else:
        print("  The start word and the target word couldn't be found. Please try again or enter -1 to continue")
        enterwords(lines)
        break


words_not = forbidden_words()  # Create a list of not allowed words
path = [start]  # words path
path_short = []  # shortest words path
seen = {start: True}  # checked words

# --the words will be seen only if they are not in the list of not allowed words
for word in words_not:  # iterate words in the list of not allowed words called words_not
    seen[word] = True  # add word to the list of checked words
# ------------------

flag = path_list()  # Ask about the length of the paths that the user wants to find


answer = input("\nDo you want to get the shortest path?(0: No / Other: Yes): ")  # check the answer
if (answer != "0"):
    if (flag): # if the user wants to see additional paths with a certain length
        print(" *List of additional paths: ")

    if find_opt(start, words, seen, target, path, path_short, flag):  # check if there is a short path
        print("  Shortest path was found")  # the shortest path was found
        print("  Number of steps: ", (len(path_short) - 1), "\n  Path: ", path_short)  # Print the path and the number of steps
    else:
        print("  No path was found")
else:
    if (flag): #new!
        print(" *List of additional paths") # new!
    if find(start, words, seen, target, path, flag):  # look for a path that goes from start to target
        #path.append(target)  # append the target word to the end
        print("\n  Path found:")  # a normal path was found
        print("  Number of steps: ", (len(path) - 1), "\n  Path: ", path)  # Print the path and the number of steps in the path
    else:
        print("  No path was found")
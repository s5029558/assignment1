
#Assignment 1 - Software Technologies - 7810ICT

#Walter Meza Almeida s5029558
#Mauricio Gonzalez Betancur s5030962


import unittest
import Functions


class TestFunctions(unittest.TestCase):

    def test_isFile1(self):  # Check if the input file name is valid.
        self.assertTrue(Functions.isFile("dictionary.txt"))

    def test_isFile2(self):  # Check if the input file name is valid
        self.assertFalse(Functions.isFile("sdfsdfsdfs"))

    def test_isFile3(self):  # Check if the input file name is valid
        self.assertFalse(Functions.isFile("dictionary"))

    def test_isFile4(self):  # Check if the input file name is valid
        self.assertFalse(Functions.isFile(""))

    def test_isValid1(self):  # Check if the words in variables words are in dictionary
        self.assertTrue(Functions.isValid({"Hide": True, "Seek": True}, ["Hide", "Seek"]))

    def test_isValid2(self):  # Check if the words in variables words are in dictionary
        self.assertFalse(Functions.isValid({"Hide": True, "Seek": True}, ["sdfsdfs", "Seek"]))

    def test_isValid3(self):  # Check if the words in variables words are in dictionary
        self.assertFalse(Functions.isValid({"Hide": True, "Seek": True}, ["dsfsd", "Sk"]))

    def test_same1(self):  # Returns the number of matching letters between item and target
        self.assertEqual(Functions.same("Hide", "Hidi"), 3)

    def test_same2(self):  # Returns the number of matching letters between item and target
        self.assertEqual(Functions.same("Hide", "Hgle"), 2)

    def test_same3(self):  # Returns the number of matching letters between item and target
        self.assertEqual(Functions.same("Hide", "Hfgo"), 1)

    def test_build(self):  # Returns a list of all word matching patterns
        self.assertEqual(["Hola", "Hala"],
                         Functions.build("H.la", ["Hola", "Chao", "Hala", "Malo", "Hila", "Hhla"], {"Hila": True},
                                         ["Hhla"]))

    def test_forbidden_words1(self):  # return a list of excluded words
        self.assertIsNotNone(Functions.forbidden_words())

    def test_forbidden_words2(self):  # return a list of excluded words
        self.assertIn("site", Functions.forbidden_words())

    def test_forbidden_words3(self): # return a list of excluded words
            self.assertNotIn("sits", Functions.forbidden_words())

    def test_path_list1(self):  # return the length of the path
        self.assertTrue(Functions.path_list())

    def test_path_list2(self):  # return the length of the path
        self.assertFalse(Functions.path_list())

    def test_find(self):  # find a word that differs in one letter and matches with the target word
        self.assertTrue(Functions.find("lead", ["load", "goad"], {"lead": True}, "gold", ["lead"], False))

    def test_find_opt(self):  # find a word that differs in one letter and matches target word
        self.assertTrue(
            Functions.find_opt("hide", ["side", "site", "sits", "size"], {"hide": True}, "seek", ["hide"], ["hide"], 0))


if __name__ == "__main__":
    unittest.main()
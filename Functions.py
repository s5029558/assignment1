
#Assignment 1 - Software Technologies - 7810ICT

#Walter Meza Almeida s5029558
#Mauricio Gonzalez Betancur s5030962


import re
import os.path


def isFile(file_name):
    """ Check whether the input file_name is an existing file
          : Parameter file_name: String with the file name
          : Return true if there is a file. Otherwise, return false
    """
    return os.path.isfile(file_name)  # check if file_name is a file


def isValid(dict, words):
    """ Check if the words in words are in the dictionary
        	: Parameter dict: dictionary of words allowed
        	: Parameter words: list of words to check
        	: Return: True if a word was found, otherwise False
    """
    for word in words:  # iterate the list of words
        if (word not in dict):  # If word is not in dictionary
            return False
    return True


def same(item, target):
    """ Returns the number of matching letters between item and target
          : Parameter item: string word to compare with target
          : Parameter target: string comparative word with item
          : return: number of matching letters between item and target
    """
    return len([c for (c, t) in zip(item, target) if c == t])


def build(pattern, words, seen, list):
    """ Returns a list of all words matching the pattern
         : Parameter pattern: string or pattern word
         : Parameter words: list of possible words
         : Parameter seen: list of examined scans
         : Parameter list: list of not included words
         : Return: list of words that match  a pattern
    """
    return [word for word in words
            if re.search(pattern, word) and word not in seen.keys() and
            word not in list]


def find(word, words, seen, target, path, flag):
    """ Look for one word that differs in only one character from the previous word
          : Parameter word: initial word string
          : Parameter words: list of possible string words
          : Parameter seen: list of string words that are checked
          : Parameter target: target string word
          : Parameter path:  list of string words path
         : Parameter flag: boolean of the user's answer
          : Return: Boolean True if a word was found, otherwise False
    """
    list = []  # list of possible words
    for i in range(len(word)):  # iterate until the length of the starting word
        list += build(word[:i] + "." + word[i + 1:], words, seen, list)  # Get list of possible words
    if len(list) == 0:  # if no possible words were found return false
        return False
    list = sorted([(same(w, target), w) for w in list])  # Sort word list that matches target
  #  list.reverse()  # sort the list in descending order
    for (match, item) in list:  # find the list of possible words
        if match >= len(target) - 1:  # if it matches all the characters except one in target word
            if match == len(target) - 1:  # if it matches all the characters except one in target word
                path.append(item)  # append the last word to the path
                path.append(target) # finally, append the target word
                # -- at this stage, the program should retrieve information about created paths
                if (flag):
                    print("  ", path)
                #--
            return True
        seen[item] = True  # add word to words examined
    for (match, item) in list:  # iterate the list of possible words
        path.append(item)  # add word to  path
        if find(item, words, seen, target, path, flag):  # search the next word
            return True  # if find a word
        path.pop()  # delete the last word


def forbidden_words():
    """ Add a list of words that are not allowed
        : return: a list of string words that are not allowed
    """
    words_not = []  # List of not allowed words
    answer = input(
        "\nDo you want to exclude words from the path? (0: NO / Other: Yes): ")  # get the answer
    if (answer != "0"):  # the answer is yes if the input is 0
        while "-1" not in words_not:  # whilst the user do not input -1
            words_not.append(input("  Insert the words. Enter -1 to exit: "))  # add forbidden word
        words_not.remove("-1")  # remove -1 from the list
        print("   The following words will be excluded: ", words_not)
    else:
        print("  All words will be used")
    return words_not

def enterwords(lines) -> list:
    """ Ask the user for two string inputs that correspond to the start word and the target word
        : Parameter lines: Will use each stripped line to find coincidences and return a list of possible string words
        : return: a list compound by the start string word, string target word and a list of possible string words
    """
    while True:  # infinite loop that only ends with an input from the user
        start = input("Enter start word: ")  # input start word
        words = []
        """Look for words with the same length as the start word """
        for line in lines:  # Iterate each line in file
            word = line.rstrip() # word is a copy of a stripped line
            if len(word) == len(start):  # Check if the start word's lenght is equal to the target word lenght
                words.append(word) # add the word to the list of words
        target = input("Enter target word: ")  # the user inputs the target word
        list = [start, target, words]
        return list

def find_opt(word, words, seen, target, path, path_short, flag):
    """ Look for one word that differs in only one character from the previous word
        Look for the shortest path to the target word
        : parameter word: start string word
        : parameter words: list of possible string words
        : parameter seen: list of the string words that were checked
        : parameter target: target string word
        : parameter path: list created path
        : parameter path_short: list of string words path
        : parameter flag: Boolean of the user's answer
        : return: Boolean True if a word were found, otherwise False
   """
    seen_act = {}  # dictionary of words that is already checked
    for item in seen: seen_act[item] = True  # create current checked word dictionary
    list = []  # list of possible words
    for i in range(len(word)):  # iterate until length of the start word
        list += build(word[:i] + "." + word[i + 1:], words, seen_act, list)  # get list of possible words
    if len(list) == 0:  # if no possible words were found, return false
        return False
    list = sorted([(same(w, target), w) for w in list])  # Order list of words by coincidences with target
    list.reverse()  # order list in descending order
    for (match, item) in list:  # iterate the list of possible words
        if match >= len(target) - 1:  # check if all the characters match with target except one
            if match == len(target) - 1:  # check if all the characters match with target except one
                path.append(item)  # append the last word to the path of words
                path.append(target)  # append target word
                # -- at this stage, it can retrieve all the paths with the specified length
                if (flag):
                    print("  ", path, "\n")
                # --
            return True
        seen[item] = True  # add word to the checked words
    for (match, item) in list:  # iterate the list of possible words
        if (len(path_short) == 0 or len(path) + 3 < len(
                path_short)):  # check if there is no shorter path or the length of the path is shorter
            path.append(item)  # append the word to the path
            seen_act[item] = True
        else:
            continue
        if find_opt(item, words, seen_act, target, path, path_short, flag):  # check if there is a path
            if (len(path_short) == 0):  # check if there is no shorter path yet
                for item in path: path_short.append(item)  # create shorter path from current path
                path.pop()  # delete target word from the list
            elif (target in path and len(path) < len(
                    path_short)):  # check if the target word is in the path, and this is shorter than the shortest path
                path_short.clear()  # eliminate shortest path
                for item in path: path_short.append(item)  # create a shorter path than the current path
            else:
                path.pop()  # else, eliminate the last word in the path
                continue
            path.pop()  # eliminate the last word in the path
        path.pop()  # eliminate the penultimate word in the path
    if (len(path_short) > 0):  # check whether there is a shorter path
    
        return True

def path_list():
    """ Ask if the user wants to get all possible paths
      : return: Boolean True if the answer is yes or False if the answer is no
    """
    flag = False  # the answer is no by default
    answer = input("\nDo you want to get a list of all possible paths? (0: No / Other: Yes): ")  # question
    if (answer != "0"): # if answers is yes
        flag = True
        print("  Your final output will also include a list of all possible paths")
    else:
        print("  A list of all possible paths is not required")
    return flag